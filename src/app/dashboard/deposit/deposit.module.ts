import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { TableComponent } from './deposit.component';

@NgModule({
    imports: [
      BrowserModule,
      RouterModule,
    FormsModule],
    declarations: [TableComponent],
    exports: [TableComponent]
})

export class TableModule { }
