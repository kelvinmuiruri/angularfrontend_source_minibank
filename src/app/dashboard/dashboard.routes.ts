import { Route } from '@angular/router';

import { HomeRoutes } from './home/home.routes';
import { ChartRoutes } from './balance/balance.route';
import { BlankPageRoutes } from './withdrawal/withdrawal.routes';
import { TableRoutes } from './deposit/deposit.routes';
import { FormRoutes } from './setting/setting.routes';
import { DashboardComponent } from './index';

export const DashboardRoutes: Route[] = [
    {
      path: 'account',
      component: DashboardComponent,
      children: [
        ...HomeRoutes,
        ...ChartRoutes,
        ...TableRoutes,
        ...BlankPageRoutes,
        ...FormRoutes
      ]
    }
];
