import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';



import { UseraccountService } from './../../../app/shared/name-list/useraccount.service';
import { Transactions } from './../../../app/dashboard/model/transactions';
/**
*  This class represents the lazy loaded HomeComponent.
*/



@Component({
  selector: 'app-home-cmp',
  templateUrl: 'home.component.html',
    styleUrls: ['timeline.scss'],
})

export class HomeComponent  implements OnInit{
transactions: Transactions[];
 errorMessage: String;
    
constructor(private accountService: UseraccountService) {
}
   ngOnInit() {
this.fetchtransaction();
}
//  get transaction all
    fetchtransaction(): void {
  this.accountService.getTransactions()
 .subscribe( transactions => this.transactions = transactions,
   error => this.errorMessage = <any>error);
   }
  
}
