
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';


import { ChartComponent } from './balance.component';

@NgModule({
    imports: [BrowserModule,
      RouterModule],
    declarations: [ChartComponent],
    exports: [ChartComponent]
})

export class ChartModule { }
