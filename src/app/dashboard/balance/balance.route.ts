import { Route } from '@angular/router';

import { ChartComponent } from './index';

export const ChartRoutes: Route[] = [
  {
    path: 'balance',
    component: ChartComponent
  }
];
