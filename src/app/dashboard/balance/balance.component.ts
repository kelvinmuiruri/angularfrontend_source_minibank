
import { Component, OnInit} from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { UseraccountService } from './../../../app/shared/name-list/useraccount.service';
import { Useraccount } from './../../../app/dashboard/model/useraccount';

@Component({
  selector: 'app-chart-cmp',
  templateUrl: 'balance.component.html'
})

export class ChartComponent implements OnInit {
useraccount: Useraccount[];
 errorMessage: String;
  
constructor(private accountService: UseraccountService) {
}
  ngOnInit() {
this.fetchuseraccountvalue();
}
 // get useraccount balance
  fetchuseraccountvalue(): void {
  this.accountService.getuseraccountWithObservable()
 .subscribe( useraccount => this.useraccount = useraccount,
   error => this.errorMessage = <any>error);
   }

}