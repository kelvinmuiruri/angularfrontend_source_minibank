/**
 * This barrel file provides the export for the lazy loaded ChartComponent.
 */
export * from './balance.component';
export * from './balance.route';
