import { Route } from '@angular/router';

import { BlankPageComponent } from './index';

export const BlankPageRoutes: Route[] = [
  {
    path: 'withdrawal',
    component: BlankPageComponent
  }
];
