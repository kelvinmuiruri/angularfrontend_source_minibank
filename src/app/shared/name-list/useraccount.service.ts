import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { Useraccount } from './../../dashboard/model/useraccount';
import  { Transactions } from './../../dashboard/model/transactions';
@Injectable()
export class UseraccountService {
 url = 'http://localhost:8080/';
 constructor(private http: Http) { }


    // get useraccount balance value
    getuseraccountWithObservable(): Observable<Useraccount[]> {
        return this.http.get("employee/api/employee/")
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
  // get Transactions all  value
    getTransactions(): Observable<Transactions[]> {
        return this.http.get("account/api/find/transactions")
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }   
  
   deposit( user){
      let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.url, user, options)
                   .map(this.extractData)
                   .catch(this.handleErrorObservable);
  }
  // Extract to
  private extractData(res: Response) {
      let body = res.json();
      return body || { };
    }
  // handle errors
     private handleErrorObservable (error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
    }
}
